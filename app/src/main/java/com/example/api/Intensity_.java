package com.example.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Intensity_ {

    @SerializedName("forecast")
    @Expose
    private Integer forecast;
    @SerializedName("actual")
    @Expose
    private Integer actual;
    @SerializedName("index")
    @Expose
    private String index;

    public Integer getForecast() {
        return forecast;
    }

    public void setForecast(Integer forecast) {
        this.forecast = forecast;
    }

    public Integer getActual() {
        return actual;
    }

    public void setActual(Integer actual) {
        this.actual = actual;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

}
