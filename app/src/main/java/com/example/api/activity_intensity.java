package com.example.api;


import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class activity_intensity extends AppCompatActivity {

    ListView lst;
    List<Datum> datas;
    ApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_intensity);

        setTitle("Intensity");
        getData();
        //lst = (ListView)findViewById(R.id.intensity_lst);
    }

    void getData() {

        apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<intensity> call = apiService.getIntensity();

        call.enqueue(new Callback<intensity>() {
            @Override
            public void onResponse(Call<intensity> call, Response<intensity> response) {
                try {
                    datas = response.body().getData();
                    Toast.makeText(activity_intensity.this, "From: " + datas.get(0).getTo() + " To: " + datas.get(0).getTo() + " Forecast: " + datas.get(0).getIntensity().getForecast() + " Actual: " + datas.get(0).getIntensity().getActual() + " Index: " + datas.get(0).getIntensity().getIndex(), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<intensity> call, Throwable t) {
                Toast.makeText(activity_intensity.this, "Server not responding", Toast.LENGTH_LONG).show();
            }
        });

        /*((retrofit2.Call) call).enqueue(new Callback<intensity>() {
            @Override
            public void onResponse(retrofit2.Call<intensity> call, Response<intensity> response) {
                List<Datum> movies = response.body().getData();
            }

            @Override
            public void onFailure(retrofit2.Call<intensity> call, Throwable t) {
                Toast.makeText(activity_intensity.this,"Server not responding",Toast.LENGTH_LONG).show();
            }
        });*/

    }
}




